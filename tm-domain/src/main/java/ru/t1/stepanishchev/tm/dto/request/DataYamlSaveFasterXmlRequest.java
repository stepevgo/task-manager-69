package ru.t1.stepanishchev.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataYamlSaveFasterXmlRequest extends AbstractUserRequest {

    public DataYamlSaveFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
