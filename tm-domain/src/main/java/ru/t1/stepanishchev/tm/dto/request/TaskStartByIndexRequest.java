package ru.t1.stepanishchev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskStartByIndexRequest extends AbstractIndexRequest {

    public TaskStartByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer index) {
        super(token, index);
    }

}