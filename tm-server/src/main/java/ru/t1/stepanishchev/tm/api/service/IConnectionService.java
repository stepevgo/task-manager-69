package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

    void close();

}