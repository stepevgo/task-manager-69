package ru.t1.stepanishchev.tm.api.service.model;

import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

@Service
public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IAbstractService<M> {

}