package ru.t1.stepanishchev.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.stepanishchev.tm.api.service.IAuthService;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;
import ru.t1.stepanishchev.tm.dto.request.AbstractUserRequest;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.exception.user.AccessDeniedException;
import ru.t1.stepanishchev.tm.exception.user.PermissionException;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AbstractEndpoint {

    @NotNull
    @Autowired(required = false)
    protected IAuthService authService;

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final SessionDTO session = authService.validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new PermissionException();
        return session;
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

}