package ru.t1.stepanishchev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.model.User;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findOneById(@NotNull final String id);

    @Nullable
    User findOneByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

}