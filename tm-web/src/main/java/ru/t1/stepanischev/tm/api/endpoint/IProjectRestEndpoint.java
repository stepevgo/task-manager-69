package ru.t1.stepanischev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.stepanischev.tm.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/projects")
public interface IProjectRestEndpoint {

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    Collection<ProjectDTO> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    );

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    );

}