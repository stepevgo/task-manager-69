package ru.t1.stepanischev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.stepanischev.tm.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskRestEndpoint {

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    Collection<TaskDTO> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    );

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    TaskDTO save(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    );

}
