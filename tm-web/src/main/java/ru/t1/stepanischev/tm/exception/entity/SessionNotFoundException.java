package ru.t1.stepanischev.tm.exception.entity;

public class SessionNotFoundException extends AbstractEntityNotFoundException {

    public SessionNotFoundException() {
        super("Error! Session not found...");
    }

}