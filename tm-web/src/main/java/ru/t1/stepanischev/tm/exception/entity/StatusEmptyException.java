package ru.t1.stepanischev.tm.exception.entity;

public class StatusEmptyException extends AbstractEntityNotFoundException {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}