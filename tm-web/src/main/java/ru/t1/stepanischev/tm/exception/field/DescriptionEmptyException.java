package ru.t1.stepanischev.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldException {

    public DescriptionEmptyException() {
        super("Error! Description not found...");
    }

}