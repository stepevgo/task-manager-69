package ru.t1.stepanischev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "tm_user")
public class User {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String login;

    @Nullable
    @Column(name = "password")
    private String passwordHash;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

}