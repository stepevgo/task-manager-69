package ru.t1.stepanischev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.stepanischev.tm.api.repository.ITaskWebRepository;
import ru.t1.stepanischev.tm.exception.entity.TaskNotFoundException;
import ru.t1.stepanischev.tm.exception.user.AccessDeniedException;
import ru.t1.stepanischev.tm.model.TaskDTO;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private ITaskWebRepository taskRepository;

    @Transactional
    public void add(@Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Transactional
    public void addByUserId(@Nullable final TaskDTO model, @Nullable final String userId) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        model.setUserId(userId);
        taskRepository.save(model);
    }

    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Nullable
    public List<TaskDTO> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return taskRepository.findAllByUserId(userId);
    }

    @Nullable
    public TaskDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    public TaskDTO findOneByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return taskRepository.findByIdAndUserId(id, userId);
    }

    @Transactional
    public void remove(@Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        taskRepository.deleteById(id);
    }

    @Transactional
    public void removeByUserId(@Nullable final TaskDTO model, @Nullable final String userId) {
        if (model == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskRepository.deleteByIdAndUserId(model.getId(), userId);
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskRepository.deleteByIdAndUserId(id, userId);
    }

    @Transactional
    public void update(@Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

}