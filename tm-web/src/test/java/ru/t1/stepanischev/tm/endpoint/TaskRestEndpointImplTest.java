package ru.t1.stepanischev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.stepanischev.tm.client.TaskRestEndpointClient;
import ru.t1.stepanischev.tm.marker.IntegrationCategory;
import ru.t1.stepanischev.tm.model.TaskDTO;

import java.util.ArrayList;
import java.util.Collection;

@Category(IntegrationCategory.class)
public class TaskRestEndpointImplTest {

    @NotNull
    private TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    private Collection<TaskDTO> tasks = new ArrayList<>();

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");

    @Before
    public void initTest() {
        client.save(task1);
        client.save(task2);
        client.save(task3);
        tasks = client.findAll();
    }

    @After
    public void clean() {
        client.delete(task1);
        client.delete(task2);
        client.delete(task3);
    }

    @Test
    public void testFindAll() {
        Collection<TaskDTO> testFind = client.findAll();
        Assert.assertEquals(tasks.size(), testFind.size());
    }

    @Test
    public void testFindById() {
        TaskDTO task = client.findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void testDelete() {
        @NotNull final String id = task2.getId();
        client.delete(task2);
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void testDeleteById() {
        @NotNull final String id = task1.getId();
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void testSave() {
        @NotNull final TaskDTO task4 = new TaskDTO("Test Task 4");
        client.save(task4);
        Assert.assertNotNull(task4);
        String taskId = task4.getId();
        TaskDTO taskTest = client.findById(taskId);
        Assert.assertNotNull(taskTest);
        client.delete(task4);
    }

}
